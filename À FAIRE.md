 À FAIRE :

- gestion des aller retours
  - les chemins peuvent être accompagnés d'un booléen AR
  - faire une classe « Chemin »

- Vérifier que tout va bien quand on précise un numéro de rue

- Plus grande carte.
  - pour le graphe et pour le xml local


- Prendre en compte le pourcentage de détour accepté par un utilisateur.


- Normaliser les clefs d'adresses




À FAIRE (un jour)

- Pour chaque rue présente dans les données de Pau à vélo, mettre un coeff dans un dico pour chaque arrête.

- Sauvegarde et chargement des données :
	     - la cyclabilité -> csv (s,t,cyclabilité de l'arête (s,t)). 


- Sauvegarde hors de osmnx pour portabilité


